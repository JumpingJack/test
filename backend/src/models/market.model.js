// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
const Sequelize = require('sequelize');
const DataTypes = Sequelize.DataTypes;

module.exports = function (app) {
  const sequelizeClient = app.get('sequelizeClient');
  const market = sequelizeClient.define('market', {
    place_id: {
      type: DataTypes.STRING
    },
    price_base: {
      type: DataTypes.STRING
    },
    price_ekp: {
      type: DataTypes.STRING
    },
    additional_pay: {
      type: DataTypes.STRING
    },
    price_var: {
      type: DataTypes.STRING
    },
    percentPay: {
      type: DataTypes.STRING
    },
    market: {
      type: DataTypes.STRING
    },
    floor: {
      type: DataTypes.STRING
    },
    status: {
      type: DataTypes.STRING
    },
    date: {
      type: DataTypes.STRING // не спрашивай меня почему так
    },
    YEvalue: {
      type: DataTypes.STRING
    },
    YEuse: {
      type: DataTypes.STRING
    }
  }, {
    hooks: {
      beforeCount(options) {
        options.raw = true;
      }
    }
  });

  market.associate = function (models) { // eslint-disable-line no-unused-vars
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
  };

  return market;
};
