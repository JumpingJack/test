// Initializes the `floors` service on path `/floors`
const createService = require('feathers-sequelize');
const createModel = require('../../models/floors.model');
const hooks = require('./floors.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'floors',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/floors', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('floors');

  service.hooks(hooks);
};
