const { authenticate } = require('@feathersjs/authentication').hooks;
const market = require('../../hooks/save_market');
const marketReturnVal = require('../../hooks/return_market');
const returnsAllVal = require('../../hooks/returnsAllVal');
// const percents = require('../../hooks/save_percents');
// const percentsReturnVal = require('../../hooks/return_percents');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [
      market()
      // percents()
    ],
    update: [],
    patch: [
      market()
      // percents()
    ],
    remove: []
  },

  after: {
    all: [
      marketReturnVal(),
      // returnsAllVal()
      // percentsReturnVal()
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
