const logger = require('winston');

module.exports = function () {
  return hook => {

    if (hook.data && hook.data.name) {
      return hook.app.service('market').find({
        query: {
          place_id: hook.data.name
        }
      }).then(res => {
        if (res[0]) {
          hook.result.infoDate = res[0].infoDate;
          hook.result.price_base = res[0].price_base;
          hook.result.price_ekp = res[0].price_ekp;
          hook.result.additional_pay = res[0].additional_pay;
          hook.result.price_var = res[0].price_var;
          hook.result.percentPay = res[0].percentPay;
          hook.result.market = res[0].market;
        }

        return hook;
      });
    }
  }
};